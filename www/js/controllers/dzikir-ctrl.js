app.controller('DzikirCtrl', function($scope, $stateParams, $state, $ionicPopup, ionicMaterialInk, $cordovaVibration) {
    //ionic.material.ink.displayEffect();
    ionicMaterialInk.displayEffect();
    $scope.dzikir = $state.params.dzikir;
    $scope.jumlah = $state.params.jumlah || 33;

    var text = document.querySelector('.dzikir-container');
    console.log(Number(window.localStorage.getItem('height')));
    text.style.height = (Number(window.localStorage.getItem('height')) - 45) + 'px';
    text.style.background = 'url(https://unsplash.it/' + window.localStorage.getItem('width') + '/' + window.localStorage.getItem('height') + '/?image=' + Math.round(Math.random() * 1024) + ') no-repeat center center';
    window.text = text;
    $scope.counter = 0;

    function setLocaleStorage() {
        var today = new Date();
        var History;
        var history = {
            date: today,
            amount: $scope.counter,
            dzikir: $scope.dzikir
        }
        if (window.localStorage.getItem('History') !== null) {
            History = JSON.parse(window.localStorage.getItem('History'));
        } else {
            History = []
        }
        console.log(History);
        History.push(history);
        console.log(History);
        window.localStorage.setItem('History', JSON.stringify(History));
    }

    $scope.addCount = function() {
        if ($scope.counter >= $scope.jumlah - 1) {
            $cordovaVibration.vibrate(1500);
            $ionicPopup.alert({
                title: 'Finish!',
                template: 'You have finish your dzikir',
            }).then(function(res) {
                if (res) {
                    setLocaleStorage();
                    $scope.counter = 0;
                    $state.go('app.menu');
                }
            });
        }
        $scope.counter += 1;

    }

    $scope.done = function() {
        $ionicPopup.confirm({
            title: 'Finish?',
            template: 'Are you sure to end your dikir?',
        }).then(function(res) {
            if (res) {
                setLocaleStorage();
                $state.go('app.menu');
            }

        });
    }

})
