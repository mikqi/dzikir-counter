app.controller('MenuCtrl', function($scope, $stateParams, $ionicPopup, $state, $ionicHistory, ionicMaterialInk) {
    //ionic.material.ink.displayEffect();
    ionicMaterialInk.displayEffect();
    $ionicHistory.clearHistory();
    var jumlah = 0;


    window.localStorage.setItem('width', window.innerWidth);
    window.localStorage.setItem('height', window.innerHeight);

    document.querySelector('.menu-container').style.background = 'url(https://unsplash.it/' + window.localStorage.getItem('width') + '/' + window.localStorage.getItem('height') +'/?image=961) no-repeat center center';

    function goTo() {
        var that = this;
        $ionicPopup.prompt({
            title: this.dzikir,
            template: 'Masukkan Jumlah Dzikir ' + this.dzikir,
            inputType: 'number',
            inputPlaceholder: 'Jumlah Dzikir'
        }).then(function(jumlah) {
            if (jumlah !== undefined) {
              $state.go('app.dzikir', { dzikir: that.dzikir, jumlah: jumlah })
            }
        });
    }
    $scope.menu = [{
        dzikir: 'Allahuakbar',
        image: 'https://unsplash.it/' + window.innerWidth + '/200/?image=' + Math.round(Math.random() * 1024),
        url: goTo
    }, {
        dzikir: 'Alhamdulllah',
        image: 'https://unsplash.it/' + window.innerWidth + '/200/?image=' + Math.round(Math.random() * 1024),
        url: goTo
    }, {
        dzikir: 'Lailahallallah',
        image: 'https://unsplash.it/' + window.innerWidth + '/200/?image=' + Math.round(Math.random() * 1024),
        url: goTo
    }, {
        dzikir: 'Astagfirullah',
        image: 'https://unsplash.it/' + window.innerWidth + '/200/?image=' + Math.round(Math.random() * 1024),
        url: goTo
    }, {
        dzikir: 'Subhanallah',
        image: 'https://unsplash.it/' + window.innerWidth + '/200/?image=' + Math.round(Math.random() * 1024),
        url: goTo
    }]
})
  .controller('BlogCtrl', function($scope, $ionicPopup, $ionicLoading, $state, $http, ionicMaterialInk) {
    ionicMaterialInk.displayEffect();
    $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
    })
    $http.get('https://islamicapps.herokuapp.com/')
      .success(function (data) {
        $ionicLoading.hide();
        data.data.forEach(function (e, i) {
          data.data[i].path = e.link.split('/')[3];
        })
        console.log(data);
        $scope.blogs = data.data;
      })
  })
  .controller('StatsCtrl', function ($scope, $state, $ionicPopup, ionicMaterialInk) {
    ionicMaterialInk.displayEffect();

    $scope.history = JSON.parse(window.localStorage.getItem('History'));

    $scope.delHistory = function () {
        $ionicPopup.confirm({
            title: 'Delete?',
            template: 'Are you sure want to delete all history?',
        }).then(function(res) {
            if (res) {
                $state.go('app.menu');
                window.localStorage.clear()
            }

        });
    }
  })
  .controller('BlogDetailCtrl', function($scope, $http, $ionicLoading, $ionicPopup, $state, $http, ionicMaterialInk) {
    ionicMaterialInk.displayEffect();
    $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
    })
    var path = $state.params.detail;
    $scope.title = path.split('-').join(' ').toUpperCase();
    $http.get('https://islamicapps.herokuapp.com/article/'+path)
      .success(function (data) {
        $ionicLoading.hide();
        $scope.content = data.data;
      })

  });
